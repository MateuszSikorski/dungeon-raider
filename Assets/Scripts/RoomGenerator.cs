﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator
{
    private GameObject wall;
    private GameObject door;

    public void LoadAssets()
    {
        door = (GameObject)Resources.Load("Prefabs/WoodenDoor");
        if (door == null)
            throw new System.Exception("Wrong path for door");

        wall = (GameObject)Resources.Load("Prefabs/Wall");
        if (wall == null)
            throw new System.Exception("Wrong path for wall");
    }

    public void Generate(Vector2 position, Vector2 size, Vector2 doorPosition)
    {
        int endX = (int)size.x / 2;
        int endY = (int)size.y / 2;
        int startX = -endX;
        int startY = -endY;

        GameObject room = new GameObject();
        room.name = "Room";
        room.transform.position = position;

        for (int x = startX; x <= endX; x++)
        {
            for (int y = startY; y <= endY; y++)
            {
                if (new Vector2(x, y) == doorPosition)
                {
                    InstantiateRoomObject(room.transform, new Vector2(x, y), door);
                    continue;
                }

                if(x == startX || x == endX || y == startY || y == endY)
                {
                    InstantiateRoomObject(room.transform, new Vector2(x, y), wall);
                }
            }
        }
    }

    private void InstantiateRoomObject(Transform parent, Vector2 position, GameObject objectType)
    {
        GameObject roomObject = (GameObject)GameObject.Instantiate(objectType);
        roomObject.transform.parent = parent;
        roomObject.transform.localPosition = position;
    }
}
