﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[System.Serializable]
public class InventoryList
{
    public List<InventoryData> items;

    public InventoryList()
    {
        items = new List<InventoryData>();
    }
}

    public class InventorySaver
{
    public InventoryList items;

    public InventorySaver()
    {
        items = new InventoryList();
    }

    public void AddItem(InventoryItem item, string className, string id)
    {
        for (int i = 0; i < items.items.Count; i++)
        {
            if (items.items[i].id == id)
                return;   // TODO: throw error
        }

        IInputOutput element = item as IInputOutput;
        if (element != null)
        {
            string data = element.Save();
            items.items.Add(new InventoryData(id, className, data));
        }
    }

    public void RemoveItem(string id)
    {
        for (int i = 0; i < items.items.Count; i++)
        {
            if (items.items[i].id == id)
            {
                items.items.RemoveAt(i);
                return;
            }
        }
    }

    public void Save()
    {
        string path = "Assets/Resources/items.txt";
        var sr = File.CreateText(path);
        string json = JsonUtility.ToJson(items);
        sr.Write(json);
        sr.Close();
        AssetDatabase.Refresh();
    }

    public void Load()
    {
        string path = "items";
        TextAsset data = (TextAsset)Resources.Load(path);
        if (data != null)
        {
            InventoryList itemList = JsonUtility.FromJson<InventoryList>(data.text);
            items = itemList;
        }
    }

    public List<string> GetItemsIdList()
    {
        List<string> idList = new List<string>();
        for (int i = 0; i < items.items.Count; i++)
            idList.Add(items.items[i].id);

        return idList;
    }

    public InventoryItem GetItem(string id)
    {
        InventoryItem loadedItem = null;
        for (int i = 0; i < items.items.Count; i++)
        {
            if (id == items.items[i].id)
            {
                loadedItem = CreateObject(items.items[i].className);
                IInputOutput element = loadedItem as IInputOutput;
                if (element != null)
                    element.Load(items.items[i].itemData);
            }
        }

        return loadedItem;
    }

    private InventoryItem CreateObject(string className)
    {
        switch (className)
        {
            case "InventorySword":
                return new InventorySword();
            case "InventoryKey":
                return new InventoryKey();
        }

        //TODO: throw error
        return null;
    }
}
