﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteWrapper : ScriptableObject
{
    public Sprite[] sprites;
}
