﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentItemView : MonoBehaviour
{
    public Image icon;

    public void SetIcon(Sprite newIcon)
    {
        icon.sprite = newIcon;
        if (newIcon == null)
            icon.gameObject.SetActive(false);
        else
            icon.gameObject.SetActive(true);
    }
}
