﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackpackElementView : MonoBehaviour
{
    private Color idleColor = Color.black;
    private Color activeColor = Color.red;

    public Image background;
    public Image itemIcon;

    public void SetItemIcon(Sprite icon)
    {
        if (icon == null)
            itemIcon.gameObject.SetActive(false);
        else
        {
            itemIcon.sprite = icon;
            itemIcon.gameObject.SetActive(true);
        }
    }

    public void Activate()
    {
        background.color = activeColor;
    }

    public void Deactivate()
    {
        background.color = idleColor;
    }
}
