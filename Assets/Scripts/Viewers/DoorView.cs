﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorView : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite openDoorSprite;
    public Sprite closeDoorSprite;

    public void SetOpenDoor()
    {
        spriteRenderer.sprite = openDoorSprite;
    }

    public void SetCloseDoor()
    {
        spriteRenderer.sprite = closeDoorSprite;
    }
}
