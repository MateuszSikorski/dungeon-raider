﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestView : MonoBehaviour
{
    private const int listLength = 3;
    private BackpackElementController[] elementsList;
    public GameObject backpackElementTemplate;
    public Transform listParentObject;

    public void CreateList()
    {
        elementsList = new BackpackElementController[listLength];

        for (int i = 0; i < listLength; i++)
        {
            GameObject newElement = GameObject.Instantiate(backpackElementTemplate, listParentObject) as GameObject;
            newElement.SetActive(true);
            BackpackElementController newElementController = newElement.GetComponent<BackpackElementController>();
            elementsList[i] = newElementController;
        }
    }

    public void Refresh(Backpack inventory)
    {
        List<InventoryItem> itemList = inventory.ItemList;
        for (int i = 0; i < elementsList.Length; i++)
        {
            if (i < itemList.Count)
                elementsList[i].SetItem(itemList[i]);
            else
                elementsList[i].SetItem(null);

            elementsList[i].Deacvtivate();
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
