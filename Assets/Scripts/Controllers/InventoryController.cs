﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public BackpackWindowController backpackController;
    public ItemDescriptionWindowController descriptionWindow;
    public EquipmentWindowController equipmentWindow;

    public void Init()
    {
        backpackController.Init();
        equipmentWindow.Init();
    }

    public void OpenOrCloseInventoryWindow()
    {
        if (gameObject.activeSelf)
            gameObject.SetActive(false);
        else
        {
            RefrehAll();
            gameObject.SetActive(true);
        }
    }

    public void RefrehAll()
    {
        backpackController.Refresh();
        equipmentWindow.Refresh();
        descriptionWindow.Item = null;
    }
}
