﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour, IInteractiveObject
{
    private InventoryKey inventoryKey;

    public string keyName;
    public string keyDescription;
    public Sprite keyInventorySprite;
    public InventoryKeyType keyType;

    private void Awake()
    {
        inventoryKey = new InventoryKey(keyType);
        inventoryKey.description = keyDescription;
        inventoryKey.name = keyName;
        inventoryKey.inventorySprite = keyInventorySprite;
    }

    public void Use(Backpack inventory)
    {
        inventory.Add(inventoryKey);
        Destroy(gameObject);
    }
}
