﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDescriptionWindowController : MonoBehaviour, IWindow
{
    private InventoryItem item;

    public Text descriptionText;

    public InventoryItem Item
    {
        set
        {
            item = value;
            Refresh();
        }
    }

    public void Refresh()
    {
        if (item != null)
            descriptionText.text = item.GetDescription();
        else
            descriptionText.text = string.Empty;
    }
}
