﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackpackWindowController : MonoBehaviour, IWindow
{
    private const int listLength = 3;
    private BackpackElementController[] elementsList;
    private BackpackElementController activeElement;

    public InventoryController controller;
    public Transform listParentObject;
    public GameObject backpackElementTemplate;

    public BackpackElementController ActiveElement
    {
        set
        {
            activeElement = value;
            Refresh();
            controller.descriptionWindow.Item = activeElement.ElementItem;
        }
        get
        {
            return activeElement;
        }
    }

    public void Init()
    {
        CreateList();
    }

    private void CreateList()
    {
        elementsList = new BackpackElementController[listLength];

        for (int i = 0; i < listLength; i++)
        {
            GameObject newElement = GameObject.Instantiate(backpackElementTemplate, listParentObject) as GameObject;
            newElement.SetActive(true);
            BackpackElementController newElementController = newElement.GetComponent<BackpackElementController>();
            newElementController.Controller = this;
            elementsList[i] = newElementController;
        }
    }

    public void Refresh()
    {
        List<InventoryItem> itemList = GameController.instance.playerController.Backpack.ItemList;
        for (int i = 0; i < elementsList.Length; i++)
        {
            if (i < itemList.Count)
                elementsList[i].SetItem(itemList[i]);
            else
                elementsList[i].SetItem(null);

            elementsList[i].Deacvtivate();
        }
    }
}
