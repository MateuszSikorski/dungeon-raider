﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentWindowController : MonoBehaviour, IWindow
{
    public EquipmentItemView weaponView;
    public InventoryController controller;

    public void Init()
    {
        weaponView.SetIcon(null);
    }

    public void Refresh()
    {
        Equipment playerEquipment = GameController.instance.playerController.Equipment;
        InventoryWeapon weapon = playerEquipment.Weapon;
        if (weapon != null)
            weaponView.SetIcon(weapon.equipmentSprite);
        else
            weaponView.SetIcon(null);
    }

    public void WeaponButtonAction()
    {
        BackpackWindowController backpack = controller.backpackController;
        BackpackElementController backpackElement = backpack.ActiveElement;
        if (backpackElement != null)
        {
            Equipment playerEquipment = GameController.instance.playerController.Equipment;
            if (playerEquipment.CanEquipWeapon(backpackElement.ElementItem))
            {
                playerEquipment.EquipWeapon(backpackElement.ElementItem);
                Refresh();
            }
        }
    }
}
