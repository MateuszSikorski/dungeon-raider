﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackpackElementController : MonoBehaviour
{
    private InventoryItem elementItem;
    private BackpackWindowController controller;

    public BackpackElementView elementView;

    public InventoryItem ElementItem
    {
        get
        {
            return elementItem;
        }
    }

    public BackpackWindowController Controller
    {
        set
        {
            controller = value;
        }
    }

    public void Activate()
    {
        if (elementItem != null)
        {
            controller.ActiveElement = this;
            elementView.Activate();
        }
    }

    public void Deacvtivate()
    {
        elementView.Deactivate();
    }

    public void SetItem(InventoryItem item)
    {
        elementItem = item;
        if (elementItem != null)
            elementView.SetItemIcon(item.inventorySprite);
        else
            elementView.SetItemIcon(null);
    }
}
