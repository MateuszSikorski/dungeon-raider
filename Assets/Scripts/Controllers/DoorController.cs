﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour, IInteractiveObject
{
    private enum DoorState
    {
        Open,
        Close,
        Lock
    }

    private DoorState state;

    public DoorView view;
    public BoxCollider2D blockCollider;
    public bool locked;
    public InventoryKeyType keyType;

    private void Awake()
    {
        state = DoorState.Close;
    }

    public void Use(Backpack inventory)
    {
        UnlockDoor(inventory);
        OpenOrCloseDoor();
    }

    private void OpenOrCloseDoor()
    {
        if (!locked)
        {
            if (state == DoorState.Close)
            {
                state = DoorState.Open;
                blockCollider.enabled = false;
                view.SetOpenDoor();
            }
            else
            {
                state = DoorState.Close;
                blockCollider.enabled = true;
                view.SetCloseDoor();
            }
        }
    }

    private void UnlockDoor(Backpack inventory)
    {
        if (locked)
        {
            InventoryKey key = new InventoryKey(keyType);
            if (inventory.Contains(key))
            {
                locked = false;
                inventory.Remove(key);
            }
        }
    }
}
