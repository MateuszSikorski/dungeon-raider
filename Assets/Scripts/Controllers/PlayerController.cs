﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private enum PlayerState
    {
        Idle,
        Use
    }

    private PlayerState state;
    private Backpack backpack;
    private Equipment equipment;

    public Rigidbody2D rigidBody;
    public float speed;

    public Backpack Backpack
    {
        get
        {
            return backpack;
        }
    }

    public Equipment Equipment
    {
        get
        {
            return equipment;
        }
    }

    public void Move(Vector3 direction)
    {
        rigidBody.velocity = direction * speed;
    }

    public void UseAction()
    {
        state = PlayerState.Use;
    }

    private void Awake()
    {
        state = PlayerState.Idle;
        backpack = new Backpack();
        equipment = new Equipment();
    }

    private void Update ()
    {
        ResetForUpdate();
        KeyboardControl();
	}

    private void ResetForUpdate()
    {
        rigidBody.velocity = Vector3.zero;
        state = PlayerState.Idle;
    }

    private void KeyboardControl()
    {
        if (Input.GetKey(KeyCode.DownArrow))
            Move(Vector3.down);
        if (Input.GetKey(KeyCode.UpArrow))
            Move(Vector3.up);
        if (Input.GetKey(KeyCode.LeftArrow))
            Move(Vector3.left);
        if (Input.GetKey(KeyCode.RightArrow))
            Move(Vector3.right);
        if (Input.GetKeyDown(KeyCode.E))
            UseAction();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        IInteractiveObject interactiveObject = collision.gameObject.GetComponent<IInteractiveObject>();
        if (interactiveObject != null && state == PlayerState.Use)
        {
            interactiveObject.Use(backpack);
        }
    }
}
