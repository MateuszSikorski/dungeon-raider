﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour, IInteractiveObject
{
    private InventorySword inventorySword;

    public string swordName;
    public string swordDescription;
    public Sprite swordInventorySprite;
    public Sprite swordEquipmentSprite;
    public float swordWeight;
    public int swordDamage;

    private void Awake()
    {
        inventorySword = new InventorySword();
        inventorySword.description = swordDescription;
        inventorySword.name = swordName;
        inventorySword.inventorySprite = swordInventorySprite;
        inventorySword.equipmentSprite = swordEquipmentSprite;
        inventorySword.weight = swordWeight;
        inventorySword.damage = swordDamage;
    }

    public void Use(Backpack inventory)
    {
        inventory.Add(inventorySword);
        Destroy(gameObject);
    }
}
