﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour, IInteractiveObject
{
    private Backpack targetInventory;
    private Chest chestData;
    public Sprite keySprite;
    public ChestView view;

    void Start()
    {
        InventoryKey newKey = new InventoryKey(InventoryKeyType.GoldKey);
        newKey.inventorySprite = keySprite;
        chestData = new Chest();
        chestData.Add(newKey);

        view.CreateList();
        view.Refresh(chestData.Inventory);
    }

    public void Use(Backpack inventory)
    {
        targetInventory = inventory;
        view.Open();
        view.Refresh(chestData.Inventory);
    }
}
