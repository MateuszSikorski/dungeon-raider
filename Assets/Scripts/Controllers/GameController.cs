﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    private RoomGenerator roomGenerator;

    public PlayerController playerController;
    public GUIController guiController;

    private void Init()
    {
        instance = this;
        roomGenerator = new RoomGenerator();
        guiController.Init();
    }

    void Start ()
    {
        Init();
        //GenerateRoom();
	}

    private void GenerateRoom()
    {
        try
        {
            Vector2 roomSize = new Vector2(7, 7);
            roomGenerator.LoadAssets();
            roomGenerator.Generate(new Vector2(0, 0), roomSize, new Vector2(0, (int)roomSize.y / 2));
        }
        catch (System.Exception error)
        {
            Debug.LogError(error.Message);
        }
    }
}
