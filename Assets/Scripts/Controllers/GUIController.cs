﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    public InventoryController inventoryWindow;

    public void Init()
    {
        inventoryWindow.Init();
    }

    public void OpenOrCloseInventoryWindow()
    {
        inventoryWindow.OpenOrCloseInventoryWindow();
    }
}
