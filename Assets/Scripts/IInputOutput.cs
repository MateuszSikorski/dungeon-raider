﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputOutput
{
    string Save();
    void Load(string data);
}
