﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class InventoryItem
{
    public string name;
    public string description;
    public Sprite inventorySprite;
    public float weight;

    public virtual bool IsEqual(InventoryItem item)
    {
        return false;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    protected virtual void EditorGUI()
    {
#if UNITY_EDITOR
        EditorGUIInventoryData();
        EditorGUIInventoryIcon();
#endif
    }

    protected void EditorGUIInventoryIcon()
    {
#if UNITY_EDITOR
        inventorySprite = (Sprite)EditorGUILayout.ObjectField("Inventory sprite", inventorySprite, typeof(Sprite));
#endif
    }

    protected virtual void EditorGUIInventoryData()
    {
#if UNITY_EDITOR
        name = EditorGUILayout.TextField("Name:", name);
        description = EditorGUILayout.TextField("Description:", description);
        weight = EditorGUILayout.FloatField("Weight:", weight);
#endif
    }

    public override string ToString()
    {
        string data = string.Format("name:{0}\ndescription:{1}\nweight:{2}\n", name, description, weight);
        data += string.Format("inventorySprite:{0}\n", inventorySprite.name);
        return data;
    }

    public virtual void Parse(string data)
    {
        LoadData(data);
        LoadSprites(data);
    }

    protected virtual void LoadData(string data)
    {
        string[] splits = data.Split(':');
        switch (splits[0])
        {
            case "name":
                name = splits[1];
                break;
            case "description":
                description = splits[1];
                break;
            case "weight":
                weight = float.Parse(splits[1]);
                break;
        }
    }

    protected virtual void LoadSprites(string data)
    {
        string[] splits = data.Split(':');
        if (splits[0] == "inventorySprite")
        {
            SpriteWrapper wrapper = Resources.Load("SpriteWrapper") as SpriteWrapper;
            for (int i = 0; i < wrapper.sprites.Length; i++)
            {
                if (wrapper.sprites[i].name == splits[1])
                    inventorySprite = wrapper.sprites[i];
            }
        }
    }
}
