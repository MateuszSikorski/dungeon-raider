﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class InventoryWeapon : InventoryEquipment
{
    public int damage;

    protected override void EditorGUI()
    {
#if UNITY_EDITOR
        EditorGUIInventoryData();
        EditorGUIIcons();
#endif
    }

    protected override void EditorGUIInventoryData()
    {
        base.EditorGUIInventoryData();
        damage = EditorGUILayout.IntField("Damage:", damage);
    }

    public override string ToString()
    {
        string data = base.ToString();
        data += string.Format("damage:{0}\n", damage);
        return data;
    }

    protected override void LoadData(string data)
    {
        base.LoadData(data);
        string[] splits = data.Split(':');
        if (splits[0] == "damage")
            damage = int.Parse(splits[1]);
    }
}
