﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment
{
    private InventoryWeapon weapon;

    public bool CanEquipWeapon(InventoryItem item)
    {
        InventoryWeapon newWeapon = item as InventoryWeapon;
        if (newWeapon != null)
            return true;

        return false;
    }

    public void EquipWeapon(InventoryItem item)
    {
        if (CanEquipWeapon(item))
            weapon = item as InventoryWeapon;
    }

    public InventoryWeapon Weapon
    {
        get
        {
            return weapon;
        }
    }
}
