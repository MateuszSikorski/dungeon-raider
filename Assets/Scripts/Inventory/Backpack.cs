﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backpack
{
    private List<InventoryItem> itemList;

    public Backpack()
    {
        itemList = new List<InventoryItem>();
    }

    public List<InventoryItem> ItemList
    {
        get
        {
            return itemList;
        }
    }

    public void Add(InventoryItem newItem)
    {
        itemList.Add(newItem);
    }

    public void Remove(int index)
    {
        itemList.RemoveAt(index);
    }

    public void Remove(InventoryItem item)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].IsEqual(item))
            {
                itemList.RemoveAt(i);
                return;
            }
        }
    }

    public bool Contains(InventoryItem item)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].IsEqual(item))
                return true;
        }

        return false;
    }
}
