﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//#if UNITY_EDTOR
using UnityEditor;
//#endif

public class InventorySword : InventoryWeapon, IEditor, IInputOutput
{
    public override bool IsEqual(InventoryItem item)
    {
        InventorySword newSword = item as InventorySword;
        if (newSword != null)
        {
            if (name != newSword.name)
                return false;
            if (description != newSword.description)
                return false;
            if (damage != newSword.damage)
                return false;
            if (inventorySprite != newSword.inventorySprite)
                return false;
            if (equipmentSprite != newSword.equipmentSprite)
                return false;

            return true;
        }

        return false;
    }

    public override string GetDescription()
    {
        return string.Format("{0}\n{1}\n\nWeight: {2}\nDamage: {3}", name, description, weight, damage);
    }

    public void EditorShow()
    {
#if UNITY_EDITOR
        EditorGUI();
#endif
    }

    public string Save()
    {
        return ToString();
    }

    public void Load(string data)
    {
        string[] splits = data.Split('\n');
        for (int i = 0; i < splits.Length; i++)
        {
            Parse(splits[i]);
        }
    }

    protected override void EditorGUI()
    {
        base.EditorGUI();
    }
}
