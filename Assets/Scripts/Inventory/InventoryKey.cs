﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum InventoryKeyType
{
    GoldKey,
    SilverKey
}

public class InventoryKey : InventoryItem, IEditor
{
    protected InventoryKeyType keyType;

    public InventoryKey()
    {
        keyType = InventoryKeyType.GoldKey;
        weight = 0f;
    }

    public InventoryKey(InventoryKeyType type)
    {
        keyType = type;
        weight = 0f;
    }

    public InventoryKeyType KeyType
    {
        get
        {
            return keyType;
        }
    }

    public override bool IsEqual(InventoryItem item)
    {
        InventoryKey newKey = item as InventoryKey;
        if (newKey != null)
        {
            return newKey.KeyType == keyType;
        }

        return false;
    }

    public override string GetDescription()
    {
        return string.Format("{0}\n\nWeight: {1}", description, weight);
    }

    public void EditorShow()
    {
#if UNITY_EDITOR
        EditorGUIInventoryData();
        keyType = (InventoryKeyType)EditorGUILayout.EnumPopup("Key type:", keyType);
        EditorGUIInventoryIcon();
#endif
    }
}
