﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryData
{
    public string id;
    public string className;
    public string itemData;

    public InventoryData()
    {
        id = string.Empty;
        className = string.Empty;
        itemData = string.Empty;
    }

    public InventoryData(string id, string className, string itemData)
    {
        this.id = id;
        this.className = className;
        this.itemData = itemData;
    }
}
