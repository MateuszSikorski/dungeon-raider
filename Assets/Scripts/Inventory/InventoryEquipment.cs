﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class InventoryEquipment : InventoryItem
{
    public Sprite equipmentSprite;

    protected override void EditorGUI()
    {
#if UNITY_EDITOR
        EditorGUIInventoryData();
        EditorGUIIcons();
#endif
    }

    protected void EditorGUIIcons()
    {
#if UNITY_EDITOR
        GUILayout.BeginHorizontal();
        EditorGUIInventoryIcon();
        equipmentSprite = (Sprite)EditorGUILayout.ObjectField("Equipment sprite", equipmentSprite, typeof(Sprite));
        GUILayout.EndHorizontal();
#endif
    }

    public override string ToString()
    {
        string data = base.ToString();
        data += string.Format("equipmentSprite:{0}\n", equipmentSprite.name);
        return data;
    }

    protected override void LoadSprites(string data)
    {
        base.LoadSprites(data);
        string[] splits = data.Split(':');
        if (splits[0] == "equipmentSprite")
        {
            SpriteWrapper wrapper = Resources.Load("SpriteWrapper") as SpriteWrapper;
            for (int i = 0; i < wrapper.sprites.Length; i++)
            {
                if (wrapper.sprites[i].name == splits[1])
                    equipmentSprite = wrapper.sprites[i];
            }
        }
    }
}
