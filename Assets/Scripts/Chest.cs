﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest
{
    protected Backpack inventory;

    public Chest()
    {
        inventory = new Backpack();
    }

    public Backpack Inventory
    {
        get
        {
            return inventory;
        }
    }

    public void Add(InventoryItem newItem)
    {
        inventory.Add(newItem);
    }

    public void Remove(int index)
    {
        inventory.Remove(index);
    }
}
