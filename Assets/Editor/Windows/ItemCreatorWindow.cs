﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class ItemCreatorWindow : EditorWindow
{
    private string[] options = { "Sword", "Key" };
    private int itemTypeIndex;
    private int itemIdIndex;
    private InventoryItem item;
    private InventorySaver itemList;
    private List<string> idList;
    private string itemId;
    private string className;

    [MenuItem("MyWindows/ItemCreator")]
    static void Init()
    {
        ItemCreatorWindow window = (ItemCreatorWindow)EditorWindow.GetWindow(typeof(ItemCreatorWindow));
        window.Show();
    }

    void Awake()
    {
        Load();
        idList = itemList.GetItemsIdList();
    }

    private void OnGUI()
    {
        itemIdIndex = EditorGUILayout.Popup(itemIdIndex, idList.ToArray());
        if (GUILayout.Button("Load"))
            GetItem();
        itemTypeIndex = EditorGUILayout.Popup(itemTypeIndex, options);

        if (GUILayout.Button("Create"))
        {
            if (itemTypeIndex == 0)
                item = new InventorySword();
            else if (itemTypeIndex == 1)
                item = new InventoryKey();
        }

        itemId = EditorGUILayout.TextField("Item ID:", itemId);
        
        if (GUILayout.Button("Save"))
            Save();

        if (item != null)
        {
            IEditor editorItem = item as IEditor;
            if (editorItem != null)
                editorItem.EditorShow();
        }
    }

    private void Load()
    {
        itemList = new InventorySaver();
        itemList.Load();
    }

    private void Save()
    {
        string path = AssetDatabase.GetAssetPath(item.inventorySprite);
        string guid = AssetDatabase.AssetPathToGUID(path);
        string pathFromGuid = AssetDatabase.GUIDToAssetPath(guid);
        itemList.AddItem(item, item.GetType().Name, itemId);
        itemList.Save();
    }

    private void GetItem()
    {
        item = itemList.GetItem(idList[itemIdIndex]);
        itemId = idList[itemIdIndex];
    }
}