﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DatabasesCreator
{
    [MenuItem("DatabasesCreator/SpriteWrapper")]
    public static void CreateSpriteWrapper()
    {
        SpriteWrapper wrapper = ScriptableObject.CreateInstance<SpriteWrapper>();
        AssetDatabase.CreateAsset(wrapper, "Assets/Resources/SpriteWrapper.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
